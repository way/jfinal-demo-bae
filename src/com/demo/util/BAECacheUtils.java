package com.demo.util;

import com.baidu.bae.api.memcache.BaeMemcachedClient;

/**
 * @author L.cm
 * <doc>http://javasdk.duapp.com/?com/baidu/bae/api/memcache/BaeMemcachedClient.html</doc>
 */
public class BAECacheUtils {

	private static BaeMemcachedClient cache = new BaeMemcachedClient();
	
	static{
		cache.setAk(ConfigUtil.get("user"));
		cache.setSk(ConfigUtil.get("password"));
	}
	
	private BAECacheUtils() {}

	public static Object get(String key) {
		return cache.get(key);
	}
	
	public static Object put(String key, Object value) {
		return cache.set(key, value);
	}
}
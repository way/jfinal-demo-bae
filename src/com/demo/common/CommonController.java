package com.demo.common;

import com.demo.util.BAECacheUtils;
import com.jfinal.core.Controller;

/**
 * CommonController
 */
public class CommonController extends Controller {
	
	public void index() {
		render("/common/index.html");
	}
	
	public void test() {
		try {
			BAECacheUtils.put("test", getPara("test"));
		} catch (Exception e) {
			e.printStackTrace();
			BAECacheUtils.put("test", "123123123123");
		}
		renderJson("back", BAECacheUtils.get("test"));
	}
}

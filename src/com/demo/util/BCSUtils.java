package com.demo.util;


import java.io.InputStream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.baidu.inf.iis.bcs.BaiduBCS;
import com.baidu.inf.iis.bcs.auth.BCSCredentials;
import com.baidu.inf.iis.bcs.model.ObjectMetadata;
import com.baidu.inf.iis.bcs.request.PutObjectRequest;

/**
 * 百度云储存上传
 * @author L.cm
 * @date 2013-5-22 下午2:19:05
 */
public class BCSUtils {
    
    private static final Log log = LogFactory.getLog(BCSUtils.class);
    // ----------------------------------------
    public static String host = "bcs.duapp.com";
    public static String bucket = "jfinaldemo";
    
    public static BaiduBCS getBaiduBCS (){
    	String accessKey = ConfigUtil.get("user");
        String secretKey = ConfigUtil.get("password");
        BCSCredentials credentials = new BCSCredentials(accessKey, secretKey);
        BaiduBCS baiduBCS = new BaiduBCS(credentials, host);
        baiduBCS.setDefaultEncoding("UTF-8"); // Default UTF-8 
        return baiduBCS;
    }
    
    /**
     * bcs-sdk-java_1.4.3.zip
     * 
     * url: http://developer.baidu.com/wiki/index.php?title=docs/cplat/stor/sdk
     * 
     * 说明 更多的 请根据 /bcs-sdk-java_1.4.3/sample/Sample.java 更改
     */
    
    /**
     * 百度云文件上传
     * @param @param content
     * @param @param contentType
     * @param @param length
     * @param @param last
     * @param @return    设定文件
     * @return boolean    返回类型
     * @throws
     */
    public static boolean uploadByInputStream(InputStream content, String contentType, long length, String fileName) {
        BaiduBCS baiduBCS = getBaiduBCS();
        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.setContentType(contentType);
        objectMetadata.setContentLength(length);
        // 设置成公开读
        objectMetadata.setHeader("x-bs-acl", "public-read");
        String object = "/" + fileName;
        PutObjectRequest request = new PutObjectRequest(bucket, object, content, objectMetadata);
        ObjectMetadata result = baiduBCS.putObject(request).getResult();
        System.out.println(result.getContentMD5());
        log.info(result);
        return true;
    }
}

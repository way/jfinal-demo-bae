# jfinal BAE云环境demo！

*删除了`BAE`已有的`jar`包，使用`BAE_eclise`可直接转成`BAE_java`项目。

`eclipse`用户可下载我整理出来的BAE环境`jar`，然后以用户`lib`导入。

[BAE_LIB.zip](http://bcs.duapp.com/jfinaldemo/BAE_LIB.zip)

demo演示地址：
[http://jfinaldemo.duapp.com/](http://jfinaldemo.duapp.com/)

# git and import to eclipse
```
 git clone http://git.oschina.net/596392912/jfinal-demo-bae.git
```

如果有不明白或问题可联系email：596392912@qq.com Thanks！

## 鸣谢
1. [JFinal](http://www.oschina.net/p/jfinal)

## License

( The MIT License )
